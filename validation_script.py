import json
import sys
import validators


with open('manifest.json') as fh:
	data = fh.read()
	json_data = json.loads(data)


field=("Object", "Repo URL", "Source URL")

for i in json_datai1:
	found=0
	print(i['Source URL'],i['Source URL'].strip(),i['Source Type'],i['Source Type'].strip())
	if i['Source URL'] is None or i['Source URL'].strip() == '' or i['Source Type'] is None or  i['Source Type'].strip() == '':
		print('Fields either Source URL or Source Type is empty')
		sys.exit(1)
	else:
		if not i['Source URL'].endswith(i['Source Type'].lower()):
			print('Rejecting Commit since URL is not ended with exact type')
			sys.exit(1)

	if not validators.url(i['Repo URL']):
		print("Not Valid URL")
		sys.exit(1)



	for value in field:
		if i[value] is None or i[value].strip == '':
			print("The fields are empty so rejecting commit")
			print('Package Details',i,"\n",'Field Name',value)
			sys.exit(1)	
	dependencies=i.get('Dependencies', '')
	if dependencies is None:
		dependencies=''
	dependencies=dependencies.split(',')
	for dependen in dependencies:
		for k in json_data:
			if dependen != k['Unique Package Name']:
				found=1
			else:
				found=0
				if k['Dependencies'] is None or k['Dependencies'] == ' ':
					print("Dependencies Field is not defined for below package",k,"\nRejecting Commits")
					sys.exit(1);
				break
	if found == 1:
		print('Dependencies are not defined for dependencies',dependen,'in Unique Package Name',i['Unique Package Name'],'\n','So Rejecting Commits')
		sys.exit(1)


