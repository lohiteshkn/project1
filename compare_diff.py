import json
import subprocess

from git import Repo

with open('manifest.json') as fh:
        data = fh.read()
        json_data = json.loads(data)



# def get_commit_changes_v1(repo_path, file_path, old_commit, new_commit):
#     repo = Repo(repo_path)
#     diff = repo.git.diff(old_commit, new_commit, file_path)

#     if not diff:
#         print('No changes found between the given commits.')
#         return []

#     changed_items = []
#     with open(file_path, 'r') as json_file:
#         data = json.load(json_file)

#     for item in data:
#         package_name = item.get('package_name')
#         version = item.get('version')
#         if package_name and version:
#             commit = repo.git.log('--pretty=format:%h', '-1', '--grep={}:{}'.format(package_name, version))
#             if commit:
#                 changed_items.append(item)

#     return changed_items


# def get_commit_changes(repo_path, file_path, old_commit, new_commit):
#     # Get the changes in the JSON file between the two commits
#     git_cmd = [
#         'git',
#         '--no-pager',
#         '-C', repo_path,
#         'diff',
#         '--name-only',
#         '--diff-filter=M',
#         '{}..{}'.format(old_commit, new_commit),
#         '--', file_path
#     ]
#     print(' '.join(git_cmd))
#     diff_output = subprocess.check_output(git_cmd).decode('utf-8').strip()
#     print(diff_output)
#     if not diff_output:
#         print('No changes found between the given commits.')
#         return []

#     # Read the JSON file and extract the changed dictionaries
#     with open(file_path, 'r') as json_file:
#         data = json.load(json_file)

#     changed_items = []
#     for item in data:
#         package_name = item.get('name')
#         version = item.get('version')
#         if package_name and version:
#             item_path = '{}/{}'.format(repo_path, file_path)
#             git_cmd = [
#                 'git',
#                 '--no-pager',
#                 '-C', item_path,
#                 'log',
#                 '--pretty=format:%h',
#                 '-1',
#                 '--grep={}:{}'.format(package_name, version)
#             ]
#             print(' '.join(git_cmd))
#             log_output = subprocess.check_output(git_cmd).decode('utf-8').strip()

#             if log_output:
#                 changed_items.append(item)

#     return changed_items

# Example usage

# def find_item_index(lst, key1, value1, key2, value2):
#     for index, item in enumerate(lst):
#         if item.get(key1) == value1 and item.get(key2) == value2:
#             return index
#     return -1  # Return -1 if the item is not found


def compare_packages_data(old_data, new_data):
    print('comparing packages')
    print(new_data)
    old_packages = {}
    new_packages = {}

    # generating dictionary with name as key and versions list as value
    for i in old_data:
        pack_name = i['name']
        if pack_name in old_packages:
            old_packages[pack_name].append(str(i['version']))
        else:
            old_packages[pack_name] = [str(i['version'])]

    for i in new_data:
        pack_name = i['name']
        if pack_name in new_packages:
            new_packages[pack_name].append(str(i['version']))
        else:
            new_packages[pack_name] = [str(i['version'])]

    print('op:', old_packages)
    print('np:', new_packages)

    changes = []
    print('comparing old to new')

    # compare old with new to get removals 
    for package, old_version in old_packages.items():
        print('running for package:', package, old_version)
        new_version = new_packages.get(package)

        if new_version:
            if sorted(new_version) == sorted(old_version):
                print('no changes found for this packag')
            else:
                non_overlapping_items = [item for item in new_version if item not in old_version]
                print('changes in commit, version added/removed', non_overlapping_items)

                for vv in non_overlapping_items:
                    idx = find_item_index(new_data, 'name', package, 'version', vv)
                    itmp = new_data[idx]
                    itmp['Action'] = 'add'
                    changes.append(itmp)
        else:
            print('this package is completely removed in current commit')

    print('=' * 50)
    print('comparing new to old')
    # compare new with old to get additons
    for package, new_version in new_packages.items():
        print('for package:', package, new_version)
        old_version = old_packages.get(package)

        if not old_version:
            print('old version not found, this is new package')
            for vv in new_version:
                idx = find_item_index(new_data, 'name', package, 'version', vv)
                itmp = new_data[idx]
                itmp['Action'] = 'add'
                changes.append(itmp)

    # print(changes)
    return changes


def find_item_index(lst, key1, value1, key2, value2):
    for index, item in enumerate(lst):
        if item.get(key1) == value1 and item.get(key2) == value2:
            return index
    return -1  # Return -1 if the item is not found


def generate_config(repo_path: str, file_path: str, current_commit: str, prev_commit: str,
                    first_entry: bool = False, additional_data: dict = None):
    if additional_data is None:
        additional_data = {}

    if first_entry:
        additional_data['Action'] = 'add'
        package_data = get_file_content_from_commit(repo_path, file_path, current_commit)

        finalized_data = []

        for i in json.loads(package_data):
            tmp = {**i, **additional_data}
            finalized_data.append(tmp)

        return finalized_data
    else:
        print('Checking for difference in commits')
        git_cmd = [
            'git',
            '--no-pager',
            '-C', repo_path,
            'diff',
            '--name-only',
            '--diff-filter=M',
            '{}..{}'.format(prev_commit, current_commit),
            '--', file_path
        ]
        print(' '.join(git_cmd))
        diff_output = subprocess.check_output(git_cmd).decode('utf-8').strip()

        if not diff_output:
            print('No diff found between the given commits.')
            return []
        else:
            print("File path",file_path)
            old_data = json.loads(get_file_content_from_commit(repo_path, file_path, current_commit))
            new_data = json_data 
           # new_data = json.loads(get_file_content_from_commit(repo_path, file_path, current_commit))
            return compare_packages_data(old_data, new_data)


def get_file_content_from_commit(repo_path: str, file_path: str, commit_hash: str):
    print(f'getting content of file `{file_path}` for commit `{commit_hash}`')
    repo = Repo(repo_path)
    commit = repo.commit(commit_hash)
    file_content = commit.tree[file_path].data_stream.read().decode('utf-8')
    return file_content or "[]"


def get_commit_count(repo_path: str):
    repo = Repo(repo_path)
    commit_count = len(list(repo.iter_commits()))
    return commit_count


def get_previous_commit_hash(repo_path: str):
    try:
        repo = Repo(repo_path)
        current_commit = repo.commit()
        previous_commit = current_commit.parents[0]
        previous_commit_hash = previous_commit.hexsha
        return previous_commit_hash
    except Exception as e:
        print('prev commit not found(', e, ')')


def get_current_commit_hash(repo_path: str):
    repo = Repo(repo_path)
    current_commit = repo.commit()
    current_commit_hash = current_commit.hexsha
    return current_commit_hash


def compare_diff(repo_path: str, file_path: str):
    commit_count = get_commit_count(repo_path)
    print('total commit count: ', commit_count)

    prev_commit = get_previous_commit_hash(repo_path)
    current_commit = get_current_commit_hash(repo_path)

    print('previous commit:', prev_commit)
    print('current commit:', current_commit)

    print('comparing commits')
    data = generate_config(repo_path, file_path, current_commit, prev_commit,
                           first_entry=True if commit_count == 1 else False)

    print('finalized')
    print(json.dumps(data, indent=2, default=str))
    return data


if __name__ == "__main__":
    repo_path = '.'
    file_path = 'm1.json'
    compare_diff(repo_path, file_path)
    # with open('m1.json') as fm: old_commit_data = json.loads(fm.read())
    # with open('m2.json') as fm: new_commit_data = json.loads(fm.read())
    # print(compare_packages_data(old_commit_data, new_commit_data))
    # print('Changed items:')
    # for item in changes:
    #     print(item)
